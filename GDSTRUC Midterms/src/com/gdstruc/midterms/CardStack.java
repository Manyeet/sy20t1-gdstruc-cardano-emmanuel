package com.gdstruc.midterms;

import java.util.LinkedList;
import java.util.ListIterator;

public class CardStack {
    private LinkedList<Card> stack;

    public CardStack()
    {
        stack = new LinkedList<Card>();
    }

    public void push(Card card)
    {
        stack.push(card);
    }

    // Using the Linked List class, create a size variable that determines how many elements are present in your Linked List.
    public void printSize() {
        ListIterator<Card> iterator = stack.listIterator();
        int counter = 0;
        while (iterator.hasNext())
        {
            iterator.next();
            counter++;
        }
        System.out.println("Number of elements in the Stack: " + counter);
    }

    public int size(){
        ListIterator<Card> iterator = stack.listIterator();
        int counter = 0;
        while (iterator.hasNext())
        {
            iterator.next();
            counter++;
        }
        return counter;
    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public Card pop()
    {
        return stack.pop();
    }

    public Card peek()
    {
        return stack.peek();
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack.listIterator();
        while(iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
}
