package com.gdstruc.midterms;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void systemPause(Scanner scanner){
        System.out.println("Hit 'Enter' to continue.");
        scanner.nextLine();
    }
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);
        int roundCounter = 1;

        CardStack discardedPile = new CardStack();
        CardStack playerHand = new CardStack();
        CardStack playerDeck = new CardStack();
        playerDeck.push(new Card(1, "Vivian"));
        playerDeck.push(new Card(2, "Imaad"));
        playerDeck.push(new Card(3, "Kunal"));
        playerDeck.push(new Card(4, "Aubrey"));
        playerDeck.push(new Card(5, "Maleeha"));
        playerDeck.push(new Card(6, "Harper"));
        playerDeck.push(new Card(7, "Chance"));
        playerDeck.push(new Card(8, "Eileen"));
        playerDeck.push(new Card(9, "Alexis"));
        playerDeck.push(new Card(10, "Stephanie"));
        playerDeck.push(new Card(11, "Ayoub"));
        playerDeck.push(new Card(12, "Anil"));
        playerDeck.push(new Card(13, "Wyatt"));
        playerDeck.push(new Card(14, "Shantelle"));
        playerDeck.push(new Card(15, "Juniper"));
        playerDeck.push(new Card(16, "Tayah"));
        playerDeck.push(new Card(17, "Lewie"));
        playerDeck.push(new Card(18, "Leonidas"));
        playerDeck.push(new Card(19, "Katy"));
        playerDeck.push(new Card(20, "Raul"));
        playerDeck.push(new Card(21, "Veronica"));
        playerDeck.push(new Card(22, "Lidia"));
        playerDeck.push(new Card(23, "Ellen"));
        playerDeck.push(new Card(24, "Darlene"));
        playerDeck.push(new Card(25, "Sila"));
        playerDeck.push(new Card(26, "Shanice"));
        playerDeck.push(new Card(27, "Viktoria"));
        playerDeck.push(new Card(28, "Annette"));
        playerDeck.push(new Card(29, "Aila"));
        playerDeck.push(new Card(30, "Campbell"));

   while(!playerDeck.isEmpty())
        {
            int randXInt = rand.nextInt(5) + 1;
            int randCommandInt = rand.nextInt(3);
            System.out.println("RNG (1-5): " + randXInt);

            // Draw x cards
            switch (randCommandInt) {
                case 0 -> {
                    // If statements, basically for grammar
                    if (randXInt > 1)
                        System.out.println("COMMAND: Draw " + randXInt + " cards.");
                    else
                        System.out.println("COMMAND: Draw " + randXInt + " card.");
                    systemPause(scanner);

                    if (randXInt > 1 && !playerDeck.isEmpty())
                        System.out.println(randXInt + " cards are drawn out from the Player Deck to the Player's hand.");
                    else
                        System.out.println(randXInt + " card is drawn out from the Player Deck to the Player's hand.");
                    systemPause(scanner);

                    for (int i = 1; i <= randXInt; i++) {
                        // if player deck IS NOT empty, continue.
                        if (!playerDeck.isEmpty())
                            playerHand.push(playerDeck.pop());
                        // if player deck IS empty, break.
                        else break;
                    }
                }
                // Discard x cards
                case 1 -> {
                    // If statements, basically for grammar
                    if (randXInt > 1)
                        System.out.println("COMMAND: Discard " + randXInt + " cards.");
                    else
                        System.out.println("COMMAND: Discard " + randXInt + " card.");
                    systemPause(scanner);

                    if (playerHand.isEmpty())
                        System.out.println("Player hand is empty. Nothing can be discarded.");
                    else if (randXInt > 1 && !playerHand.isEmpty())
                        System.out.println(randXInt + " cards are discarded from the Player's hand.");
                    else
                        System.out.println(randXInt + " card is discarded from the Player's hand.");
                    systemPause(scanner);

                    for (int i = 1; i <= randXInt; i++) {
                        // if player hand IS NOT empty, continue.
                        if (!playerHand.isEmpty())
                            discardedPile.push(playerHand.pop());
                        // if player hand IS empty, break.
                        else break;
                    }
                }
                // Get x cards from the discarded pile
                case 2 -> {
                    // If statements, basically for grammar
                    if (randXInt > 1)
                        System.out.println("COMMAND: Get " + randXInt + " cards from the Discarded Pile.");
                    else
                        System.out.println("COMMAND: Get " + randXInt + " card from the Discarded Pile.");
                    systemPause(scanner);

                    if (discardedPile.isEmpty())
                        System.out.println("Discarded pile is empty. Nothing can be drawn out.");
                    else if (randXInt > 1 && !discardedPile.isEmpty())
                        System.out.println(randXInt + " cards are drawn out from the Discarded Pile to the Player's hand.");
                    else
                        System.out.println(randXInt + " card is drawn out from the Discarded Pile to the Player's hand.");
                    systemPause(scanner);

                    for (int i = 1; i <= randXInt; i++) {
                        // if discarded pile IS NOT empty, continue.
                        if (!discardedPile.isEmpty())
                            playerHand.push(discardedPile.pop());
                        // if discarded pile IS empty, break.
                        else break;
                    }
                }
            }
            // Clear screen!
            System.out.println("ROUND " + roundCounter + " RESULTS:");
            if (playerHand.size() > 1)
                System.out.println("List of cards that the player is currently holding: " + playerHand.size() + " items");
            else
                System.out.println("List of cards that the player is currently holding: " + playerHand.size() + " item");
            playerHand.printStack();
            if (playerHand.isEmpty())
                System.out.println("NULL");
            systemPause(scanner);

            System.out.println("Number of remaining cards in the Player Deck: " + playerDeck.size());
            playerDeck.size();
            systemPause(scanner);

            System.out.println("Number of cards in the Discarded Pile: " + discardedPile.size());
            systemPause(scanner);

            roundCounter++;
        }
        if (playerDeck.isEmpty())
        {
            System.out.println("Player Deck is empty.");
            systemPause(scanner);
            System.out.println("Thanks for playing!");
            systemPause(scanner);
            System.out.println("GG!");
            systemPause(scanner);
        }
    }
}