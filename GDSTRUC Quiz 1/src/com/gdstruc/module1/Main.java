package com.gdstruc.module1;

public class Main {

    public static void main(String[] args)
    {
        int[] numbersBubble = new int[10];

        numbersBubble[0] = 35;
        numbersBubble[1] = 69;
        numbersBubble[2] = 1;
        numbersBubble[3] = 10;
        numbersBubble[4] = -50;
        numbersBubble[5] = 320;
        numbersBubble[6] = 63;
        numbersBubble[7] = 58;
        numbersBubble[8] = 26;
        numbersBubble[9] = 13;

        int[] numbersSelection = new int[10];

        numbersSelection[0] = 35;
        numbersSelection[1] = 69;
        numbersSelection[2] = 1;
        numbersSelection[3] = 10;
        numbersSelection[4] = -50;
        numbersSelection[5] = 320;
        numbersSelection[6] = 63;
        numbersSelection[7] = 58;
        numbersSelection[8] = 26;
        numbersSelection[9] = 13;

        System.out.println("Before Bubble sort:");
        printArrayElements(numbersBubble);

        bubbleSort(numbersBubble);

        System.out.println("\n\nAfter Bubble sort:");
        printArrayElements(numbersBubble);

        System.out.println("\n\nBefore Selection sort:");
        printArrayElements(numbersSelection);

        selectionSort(numbersSelection);

        System.out.println("\n\nAfter Selection sort:");
        printArrayElements(numbersSelection);
        
    }

    private static void bubbleSort(int[] arr)
    {
        for(int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for(int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i+1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for(int i = 0; i <= lastSortedIndex; i++)
            {
                if (arr[i] > arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    private static void printArrayElements(int[] arr)
    {
        for (int j : arr)
        {
            System.out.print(j + " ");
        }
    }
}
