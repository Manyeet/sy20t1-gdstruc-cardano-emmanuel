package com.gdstruc.module2;

public class Main {
     public static void main(String[] args) {

        Player hatdog = new Player(1, "Hatdog", 21);
        Player hamburger = new Player(2, "Hamburger", 420);
        Player hatcake = new Player(3, "Hatcake", 69);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(hatdog);
        playerLinkedList.addToFront(hamburger);
        playerLinkedList.addToFront(hatcake);

        // a function that removes the first element.
        // playerLinkedList.removeFront();

        // a size() function.
         // playerLinkedList.printSize();

         playerLinkedList.printList();

         // a contains() function.
         // System.out.println(playerLinkedList.printContains(new Player(1,"Hatdog",21)));

         // an indexOf() function.
         // System.out.println(playerLinkedList.printIndexOf(new Player(1, "Hatdog", 21)));
    }
}
