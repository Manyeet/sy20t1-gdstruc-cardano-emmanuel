package com.gdstruc.module2;

public class PlayerLinkedList {
    private PlayerNode head;

    public PlayerNode getHead(){
        return head;
    }

    public void addToFront(Player player) {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    // Using the Linked List class, create a function that removes the first element.
    public void removeFront(){
        PlayerNode temp = head;
        if (temp != null)
        {
            head = temp.getNextPlayer();
            // temp = null;
        }
    }

    // Using the Linked List class, create a size variable that determines how many elements are present in your Linked List.
    public void printSize() {
        PlayerNode temp = head;
        int counter = 0;
        while (temp != null)
        {
            temp = temp.getNextPlayer();
            counter++;
        }
        System.out.println("Number of elements in the Linked List: " + counter);
    }

    public boolean printContains(Player player){
        PlayerNode temp = head;
        boolean IsInContainer = false;
        while(temp != null) {
            if (player.equals(temp.getPlayer()))
            {
                IsInContainer = true;
                break;
            }
            temp = temp.getNextPlayer();
            if (temp == null)
            {
                break;
            }
        }
        return IsInContainer;
    }

    public int printIndexOf(Player player){
        PlayerNode temp = head;
        int counter = 0;
        while (temp != null){
            if (player.equals(temp.getPlayer())){
                break;
            }
            temp = temp.getNextPlayer();
            if (temp == null)
            {
                counter = -1;
                break;
            }
            counter++;
        }
        return counter;
    }

    public void printList(){
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null){
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("NULL");
    }

}
