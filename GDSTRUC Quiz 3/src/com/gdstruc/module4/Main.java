package com.gdstruc.module4;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void systemPause(Scanner scanner){
        System.out.println("Hit 'Enter' to continue.");
        scanner.nextLine();
    }
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scanner = new Scanner(System.in);
        ArrayQueue queue = new ArrayQueue(5);
        int IDCounter = 1;
        int gameCounter = 0;

        // Create a matchmaking algorithm for players.
        // 4. The program terminates when 10 games have been successfully made.
        while(gameCounter <= 10)
        {
            System.out.println("GAMES COMMENCED: " + gameCounter);
            // 1. Every turn, x players will queue for matchmaking (x = rand () 1 to 7).
            int randXInt = rand.nextInt(7) + 1;
            for (int i = 1; i <= randXInt; i++)
            {
                int randLevel = rand.nextInt(500) + 1;
                queue.enqueue(new Player(IDCounter, "Player " + IDCounter, randLevel));
                IDCounter++;
            }
            // Grammar statements
            if (randXInt <= 1)
                System.out.println(randXInt + " player has just queued up for matchmaking.");
            else
                System.out.println(randXInt + " players have just queued up for matchmaking.");
            systemPause(scanner);
            // 2. A game can be started when at least 5 players are in the queue.
            if (queue.size() >= 5)
            {
                System.out.println("Game starting...");
                System.out.println(5 + " players will now be removed from the queue to start their game.");
                systemPause(scanner);
                // 3. When a game starts, pop the first 5 players from the queue.
                for (int i = 0; i < 5; i++)
                {
                    queue.dequeue();
                }
                gameCounter++;
            }
            System.out.println("Number of players waiting in queue: " + queue.size());
            systemPause(scanner);
        }
    }
}