package com.gdstruc.module5;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Player hatdog = new Player(1, "Hatdog", 21);
        Player hamburger = new Player(2, "Hamburger", 420);
        Player hatcake = new Player(3, "Hatcake", 69);
        Player haching = new Player(4, "Haching", 911);
        Player handwashing = new Player(5, "Handwashing", 626);

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(hatdog.getUserName(), hatdog);
        hashtable.put(hamburger.getUserName(), hamburger);
        hashtable.put(hatcake.getUserName(), hatcake);
        hashtable.put(haching.getUserName(), haching);
        hashtable.put(handwashing.getUserName(), handwashing);

        // Usage of remove function
        hashtable.remove("Hatdog");
        // hashtable.remove("Hamburger");
        // hashtable.remove("Hatcake");
        // hashtable.remove("Haching");
        // hashtable.remove("Handwashing");

        // hashtable.printHashtable();

        System.out.println(hashtable.get("Hatdog"));
    }
}
