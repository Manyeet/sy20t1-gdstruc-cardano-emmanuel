package com.gdstruc.module7;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.insert(25);
        tree.insert(17);
        tree.insert(29);
        tree.insert(18);
        tree.insert(16);
        tree.insert(-5);
        tree.insert(60);
        tree.insert(55);

        // traverseInOrder() - traverses the tree using the in-order method.
        // tree.traverseInOrder();

        // traverseInOrderDescending() - traverses the tree using the in-order method but in descending order.
        // tree.traverseInOrderDescending();

        // getMin() - gets the node with the least value in the tree.
        // System.out.println(tree.getMin());

        // getMax() - gets the node with the maximum value in the tree.
        //System.out.println(tree.getMax());

        // get() - gets the node from the given value.
        // System.out.println(tree.get(9999));
    }
}