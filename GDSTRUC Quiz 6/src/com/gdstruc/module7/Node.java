package com.gdstruc.module7;

public class Node {
    private int data;
    private Node rightChild;
    private Node leftChild;

    public void insert(int value)
    {
        if(value == data)
        {
            return;
        }
        if(value < data)
        {
            if(leftChild == null)
            {
                leftChild = new Node(value);
            }
            else
            {
                leftChild.insert(value);
            }
        }
        else
        {
            if(rightChild == null)
            {
                rightChild = new Node(value);
            }
            else
            {
                rightChild.insert(value);
            }
        }
    }

    public void traverseInOrder()
    {
        if (leftChild != null)
        {
            leftChild.traverseInOrder();
        }
        System.out.println("Data: " + data);
        if(rightChild != null)
        {
            rightChild.traverseInOrder();
        }
    }

    public Node get(int value)
    {
        if(value == data)
        {
            return this;
        }
        if (value < data)
        {
            if (leftChild != null)
            {
                return leftChild.get(value);
            }
        }
        else
        {
            if (rightChild != null)
            {
                return rightChild.get(value);
            }
        }
        return null;
    }

    // Node getMin() - gets the node with the least value in the tree
    public Node getMin()
    {
        Node currentNode = this;
        while (currentNode.getLeftChild() != null)
        {
            currentNode = currentNode.getLeftChild();
        }
        return currentNode;
    }

    // Node getMax() - gets the node with the maximum value in the tree
    public Node getMax()
    {
        Node currentNode = this;
        while(currentNode.getRightChild() != null)
        {
            currentNode = currentNode.getRightChild();
        }
        return currentNode;
    }

    // void traverseInOrderDescending() - traverses the tree using the in-order method but in descending order.
    public void traverseInOrderDescending()
    {
        if (rightChild != null)
        {
            rightChild.traverseInOrderDescending();
        }
        System.out.println("Data: " + data);
        if (leftChild != null)
        {
            leftChild.traverseInOrderDescending();
        }
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                '}';
    }
    public Node(int _data)
    {
        this.data = _data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }
}
